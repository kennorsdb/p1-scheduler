//
// Created by alejo on 3/29/18.

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

#include "testeos.h"
#include <unistd.h>
#include <string.h>

void * PrintTimer(){
    while(1){
        printf("Testing");
        sleep(1);
    }
}

void mainPrinter(){
    char buffer[5000];
    pthread_t printer;
    char * com = ">>";
    printf("%s", com);
    fgets(buffer,5000,stdin);
    int rc = pthread_create(&printer, NULL, PrintTimer, NULL);

    while(strcmp(buffer,"exit\n")!=0){
        fgets(buffer,5000,stdin);
        printf("Puso: %s-", buffer);
        if(strcmp(buffer,"kill\n")==0){
            pthread_cancel(printer);
        }
    }
}


void * PrintHello(void *threadid)
{
    long tid;
    tid = (long)threadid;
    printf("Hello World! It's me, thread #%ld!\n", tid);
    pthread_exit(NULL);
}

void callthread(){
    pthread_t threads[NUM_THREADS];
    int rc;
    long t;
    for(t=0; t<NUM_THREADS; t++){
        printf("In main: creating thread %ld\n", t);
        rc = pthread_create(&threads[t], NULL, PrintHello, (void *)t);
        if (rc){
            printf("ERROR; return code from pthread_create() is %d\n", rc);
            exit(EXIT_FAILURE);
        }
    }

    /* Last thing that main() should do */
    pthread_exit(NULL);

}

void DecirHi(){
    printf("Hola perros");
}



//"/home/alejo/lines.txt"

void readAFile(char * path){
    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen(path, "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    while ((read = getline(&line, &len, fp)) != -1) {
        printf("Retrieved line of length %zu :\n", read);
        printf("%s", line);
    }

    fclose(fp);
    if (line)
        free(line);

    exit(EXIT_SUCCESS);
}

void mainReading(char * path){
    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen(path, "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    while ((read = getline(&line, &len, fp)) != -1) {
        printf("Retrieved line of length %zu :\n", read);
        printf("%s", line);
    }

    fclose(fp);
    if (line)
        free(line);

    exit(EXIT_SUCCESS);
}

