//
// Created by alejo on 3/31/18.
//

#ifndef APPROACH_TESTEOS_H
#define APPROACH_TESTEOS_H

#define NUM_THREADS     5

void * PrintHello(void *threadid);
void callthread();
void DecirHi();
void readAFile(char * path);
void * PrintTimer();
void mainPrinter();

#endif //APPROACH_TESTEOS_H
