#include "menu.h"
#include "listPCB.h"
#include <stdio.h>

void * menu(void * scheduler)
{
    SCHEDULER * sched = scheduler;
    printf("\033[H\033[J-------------------------------------------------------------------------------\n");
    printf("--   _____ _____ _____                                                       --\n");
    printf("--  |_   _|  ___/  __ \\   I N S T I T U T O   T E C N O L Ó G I C O          --\n");
    printf("--    | | | |__ | /  \\/   -------------------------------------------        --\n");
    printf("--    | | |  __|| |       ESCUELA DE COMPUTACIÓN                             --\n");
    printf("--    | | | |___| \\__/\\                                                      --\n");
    printf("--    \\_/ \\____/ \\____/                                                      --\n");
    printf("--                                                                           --\n");
    printf("-------------------------------------------------------------------------------\n");
    printf("-- Proyecto I                                                                --\n");
    printf("--                            S C H E D U L E R                              --\n");
    printf("-------------------------------------------------------------------------------\n");
    printf("-- curso:                                                                    --\n");
    printf("--       Principios de Sistemas Operativos                                   --\n");
    printf("-------------------------------------------------------------------------------\n");
    printf("--  Estudiantes                                                              --\n");
    printf("--             Alejandro Pacheco Quesada       (2015171007)                  --\n");
    printf("--             Kenneth Obando RodrÍguez        (200565484)                   --\n");
    printf("-------------------------------------------------------------------------------\n");
    printf("-- Comandos:                                                                 --\n");
    printf("--      l   Mostrar todos los procesos activos                               --\n");
    printf("--      e   Terminar todos los procesos                                      --\n");
    printf("--      h   Cambiar a scheduler HPF                                          --\n");
    printf("--      s   Cambiar a scheduler SJF                                          --\n");
    printf("--      r   Cambiar a scheduler RR                                           --\n");
    printf("--      f   Cambiar a scheduler FIFO                                         --\n");
    printf("--      m   Menú resumen                                                     --\n");
    printf("--      q   Cambiar Quantum                                                  --\n");
    printf("-------------------------------------------------------------------------------\n");

    char option = '\0';
    while( option != 'e'){
        option = '\0';
        scanf("%1c", &option);

        switch (option){
            case 'l':
                printf("Procesos activos: \n");
                printnodoPCB(sched->listaPCB);
                break;
            case 'e':
                printf("Cerrando todos los procesos: \n");
                sched->enabled = 0;
                resumen(sched);
                break;
            case 'h':
                printf("Se cambia el Scheduler a HPF: \n");
                sched->tipo = HPF;
                break;
            case 's':
                printf("Se cambia el Scheduler a SJF: \n");
                sched->tipo = SJF;
                break;
            case 'r':
                printf("Se cambia el Scheduler a RR: \n");
                sched->tipo = RR;
                break;
            case 'f':
                printf("Se cambia el Scheduler a FIFO: \n");
                sched->tipo = FIFO;
            case 'q':
                printf("Escriba el nuevo quantum: \n");
                long newQuantum;
                scanf("%ld", &newQuantum);
                sched->quantum = FIFO;
                printf("Nuevo quantum: %ld\n", newQuantum);
                break;
            case 'm':
                printf("Resumen de la Ejecución: \n");
                resumen(sched);
                break;

        }
        

    }

    return 0;
}