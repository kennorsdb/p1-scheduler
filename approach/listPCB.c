#include "listPCB.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>

void agregarPCB(SCHEDULER *sched, PCB *pcb)
{
    if (sched == NULL)
        return;

    NODO_PCB *nodoPCB = sched->listaPCB;

    if (pcb == NULL)
    {
        return;
    }
    else if (nodoPCB == NULL) // Si la lista es igual a null, la crea y agrega pcb
    {
        pthread_mutex_lock(&(sched->listaPCBMutex));
        time(&(pcb->startTime));
        nodoPCB = (NODO_PCB *)malloc(sizeof(NODO_PCB));
        nodoPCB->pcb = pcb;
        sched->PIDS++;
        nodoPCB->pcb->pid = sched->PIDS;
        nodoPCB->sig = NULL;
        nodoPCB->ant = NULL;
        sched->listaPCB = nodoPCB;
        pthread_mutex_unlock(&(sched->listaPCBMutex));
    }
    else
    {
        pthread_mutex_lock(&(sched->listaPCBMutex));
        time(&(pcb->startTime));
        NODO_PCB *t = nodoPCB;

        while (t->sig != NULL)
        {
            t = t->sig;
        }
        t->sig = (NODO_PCB *)malloc(sizeof(NODO_PCB));
        sched->PIDS++;
        pcb->pid = sched->PIDS;
        t->sig->pcb = pcb;
        t->sig->sig = NULL;
        t->sig->ant = t;
        sched->listaPCB = nodoPCB;
        pthread_mutex_unlock(&(sched->listaPCBMutex));
    }
}
void agregarPCB_END(SCHEDULER *sched, PCB *pcb)
{
    if (sched == NULL)
        return;

    NODO_PCB *nodoPCB = sched->listaPCB_END;

    if (pcb == NULL)
    {
        return;
    }
    else if (nodoPCB == NULL) // Si la lista es igual a null, la crea y agrega pcb
    {
        pthread_mutex_lock(&(sched->listaPCB_ENDMutex));
        nodoPCB = (NODO_PCB *)malloc(sizeof(NODO_PCB));
        nodoPCB->pcb = pcb;
        nodoPCB->sig = NULL;
        nodoPCB->ant = NULL;
        sched->listaPCB_END = nodoPCB;
        pthread_mutex_unlock(&(sched->listaPCB_ENDMutex));
    }
    else
    {
        pthread_mutex_lock(&(sched->listaPCB_ENDMutex));
        NODO_PCB *t = nodoPCB;
        while (t->sig != NULL)
        {
            t = t->sig;
        }
        t->sig = (NODO_PCB *)malloc(sizeof(NODO_PCB));
        t->sig->pcb = pcb;
        t->sig->sig = NULL;
        t->sig->ant = t;
        sched->listaPCB_END = nodoPCB;
        pthread_mutex_unlock(&(sched->listaPCB_ENDMutex));
    }
}
void printnodoPCB(NODO_PCB *nodoPCB)
{
    NODO_PCB *actual = nodoPCB;
    while (actual != NULL)
    {
        printf("[%ld, %ld, %ld, %ld]->", actual->pcb->pid,
               actual->pcb->burst, actual->pcb->burstActual, actual->pcb->priority);
        actual = actual->sig;
    }
    printf("[END]\n");
}

void borrarLista(NODO_PCB *nodoPCB)
{
    if (nodoPCB != NULL)
    {
        if (nodoPCB->sig != NULL)
        {
            borrarLista(nodoPCB->sig);
        }
        free(nodoPCB);
    }
}

NODO_PCB *runPCB_FIFO(NODO_PCB *nodoPCB)
{

    if (nodoPCB != NULL)
    {
        printf("Fifo Scheduler: Proceso %ld, con burst original %ld (Burst Actual: %ld) y prioridad %ld entra en ejecución\n",
               nodoPCB->pcb->pid, nodoPCB->pcb->burst, nodoPCB->pcb->burstActual, nodoPCB->pcb->priority);
        sleep(nodoPCB->pcb->burst);
        nodoPCB->pcb->burstActual = 0;
        return nodoPCB;
    }
    else
        return NULL;
}

NODO_PCB *runPCB_SJF(NODO_PCB *nodoPCB)
{

    if (nodoPCB != NULL)
    {
        NODO_PCB *t = nodoPCB;
        NODO_PCB *selected = nodoPCB;
        long minBurst = nodoPCB->pcb->burstActual;

        while (t->sig != NULL)
        {
            if (minBurst > t->sig->pcb->burstActual)
            {
                selected = t->sig;
                minBurst = selected->pcb->burstActual;
            }
            t = t->sig;
        }

        printf("SJF Scheduler: Proceso %ld, con burst original %ld (Burst Actual: %ld) y prioridad %ld entra en ejecución\n",
               selected->pcb->pid, selected->pcb->burst, selected->pcb->burstActual, selected->pcb->priority);
        sleep(selected->pcb->burst);
        selected->pcb->burstActual = 0;
        return selected;
    }
    else
        return NULL;
}

NODO_PCB *runPCB_HPF(NODO_PCB *nodoPCB)
{

    if (nodoPCB != NULL)
    {
        NODO_PCB *t = nodoPCB;
        NODO_PCB *selected = nodoPCB;
        long maxPriority = nodoPCB->pcb->priority;

        while (t->sig != NULL)
        {
            if (maxPriority < t->sig->pcb->priority)
            {
                selected = t->sig;
                maxPriority = selected->pcb->priority;
            }
            t = t->sig;
        }

        printf("HPF Scheduler: Proceso %ld, con burst original %ld (Burst Actual: %ld) y prioridad %ld entra en ejecución\n",
               selected->pcb->pid, selected->pcb->burst, selected->pcb->burstActual, selected->pcb->priority);
        sleep(selected->pcb->burst);
        selected->pcb->burstActual = 0;
        return selected;
    }
    else
        return NULL;
}

NODO_PCB *runPCB_RR(NODO_PCB *nodoPCB, long quantum)
{

    if (nodoPCB != NULL)
    {
        NODO_PCB *t = nodoPCB;

        while (t->sig != NULL && t->pcb->quantum != 0)
        {
            t = t->sig;
        }
        // Si llegamos hasta el final, hay que resetear el quantum
        if (t->sig == NULL && t->pcb->quantum != 0)
        {
            t = nodoPCB;
            t->sig->pcb->quantum = 0;
            while (t->sig != NULL)
            {
                t->sig->pcb->quantum = 0;
                t = t->sig;
            }
            //seleccionamos el primero de la lista
            t = nodoPCB;
        }
        printf("RR Scheduler: Proceso %ld, con burst original %ld (Burst Actual: %ld) y prioridad %ld entra en ejecución\n",
               t->pcb->pid, t->pcb->burst, t->pcb->burstActual, t->pcb->priority);
        // Se ejecuta el proceso
        // Si el burst Actual es menor al quantum, ejecuta todo
        if (t->pcb->burstActual <= quantum)
        {
            sleep(t->pcb->burstActual);
            t->pcb->quantum = t->pcb->burstActual;
            t->pcb->burstActual = 0;
        }
        else
        { // de lo contrario ejecuta el quantum
            sleep(quantum);
            t->pcb->burstActual -= quantum;
            t->pcb->quantum = quantum;
        }

        return t;
    }
    else
        return NULL;
}

void terminarPCB(SCHEDULER *sched, NODO_PCB *pcb)
{
    pthread_mutex_lock(&(sched->listaPCBMutex));
    // Se elimina de la Lista de Procesos
    if (pcb->sig != NULL)
        pcb->sig->ant = pcb->ant;

    if (pcb->ant != NULL)
        pcb->ant->sig = pcb->sig;
    else
    { // Es el primer elemento de la lista
        if (pcb->sig != NULL)
        {
            pcb->sig->ant = NULL;
            sched->listaPCB = pcb->sig;
        }
        else
        {
            sched->listaPCB = NULL;
        }
    }
    pthread_mutex_unlock(&(sched->listaPCBMutex));
    // Se agrega a la lista de procesos terminados
    time(&(pcb->pcb->endTime));
    sched->procesosEjecutados++;
    agregarPCB_END(sched, pcb->pcb);
    free(pcb);
}

void resumen(SCHEDULER *sched)
{   
    printf("\n   PID   TAT   WT");
    printf("\n-----------------\n");
    NODO_PCB *t = sched->listaPCB_END;
    long totalTimePCBs = 0;
    long totalWT = 0;
    long totalTAT = 0;
    long contador = 0;
    while (t != NULL)
    {
        long tat = t->pcb->endTime - t->pcb->startTime;
        long wt = tat - t->pcb->burst;
        totalTimePCBs += t->pcb->burst;
        totalWT += wt;
        totalTAT += tat; 
        printf("%5ld %5ld %5ld\n",t->pcb->pid, tat, wt);
        t = t->sig;
        contador++;
    }
    
    long tiempoTotal = (sched->timeStop - sched->timeStart);
    printf("----------------------------------------\n");
    printf("Cantidad de procesos ejecutados:       %3ld\n", sched->procesosEjecutados);
    printf("Cantidad de Segundos Totales:          %3ld\n", tiempoTotal);
    printf("Cantidad de Segundos con CPU ocioso:   %3ld\n", tiempoTotal-totalTimePCBs);
    printf("Promedio de Waiting Time:              %3.1f\n", totalWT/(float)contador);
    printf("Promedio de Turn Around Time:          %3.1f\n", totalTAT/(float)contador);
}

void *CPUScheduler(void *scheduler)
{
    // Se valida que se incluye un scheduler
    if (scheduler == NULL)
        return 0;

    SCHEDULER *sched = scheduler;
    time(&(sched->timeStart));
    sched->procesosEjecutados = 0;

    while (sched->enabled)
    {
        NODO_PCB *run = NULL;
        // Se utiliza el scheduler seleccionado
        switch (sched->tipo)
        {
        case FIFO:
            run = runPCB_FIFO(sched->listaPCB);
            break;
        case SJF:
            run = runPCB_SJF(sched->listaPCB);
            break;
        case HPF:
            run = runPCB_HPF(sched->listaPCB);
            break;
        case RR:
            run = runPCB_RR(sched->listaPCB, sched->quantum);
            break;
        }
        // Si el burst es cero, se borra de la cola de procesos
        if (run != NULL && run->pcb->burstActual == 0)
        {
            terminarPCB(sched, run);
            printnodoPCB(sched->listaPCB);
        }

        time(&(sched->timeStop));
    }
    time(&(sched->timeStop));
    return 0;
}