//
// Created by alejo on 4/8/18.
//

#include "resources.h"

void newThread(PLIST * list, PNODE * node){
    if(list->head==NULL){
        list->head = node;
        list->last = node;
    }
    else{
        PNODE * tmp = list->last;
        list->last->next = node;
        list->last = tmp->next;
    }
    list->size+=1;
}


