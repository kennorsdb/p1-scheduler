#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include "listPCB.h"
#include "menu.h"

int main()
{
    PCB p[5] = {{0, 8, 8, 3, 0},
                {0, 4, 4, 3, 0},
                {0, 1, 1, 2, 0},
                {0, 7, 7, 4, 0},
                {0, 4, 4, 1, 0}};

    SCHEDULER sched;

    sched.listaPCB = NULL;
    pthread_mutex_init(&(sched.listaPCBMutex), NULL);
    sched.listaPCB_END = NULL;
    pthread_mutex_init(&(sched.listaPCB_ENDMutex), NULL);
    sched.tipo = FIFO;
    sched.quantum = 2;
    sched.enabled = 1;
    sched.PIDS = 0;

    pthread_t menuThread;
    pthread_create( &menuThread, NULL, menu, (void *) (&sched));
    pthread_t cpuSchedThread;
    pthread_create( &cpuSchedThread, NULL, CPUScheduler, (void *) (&sched));

    for (int i = 0; i < 5; i++)
    {
        agregarPCB(&sched, &p[i]);
        printf("Thread Principal: ");
        printnodoPCB(sched.listaPCB);
        //sleep(1);
    }

   
   pthread_join( cpuSchedThread, NULL);
   pthread_join( menuThread, NULL); 


    borrarLista(sched.listaPCB_END);

    return (0);
}