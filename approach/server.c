//
// Created by alejo on 3/31/18.
//

#include "server.h"
#include <stdio.h>
#include <stdlib.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>

#include "listPCB.h"
#include "menu.h"

#define BUFFERSIZE 512
#define MAX_CLIENTS 1000

int active = 1;


int getMAx (int *tabla, int n);
void changeActive(int act);
void compactKeys (int *tabla, int *n);
void nuevoCliente (int servidor, int *clientes, int *nClientes);
void server(SCHEDULER *sched);
void nuevoProceso(char * line, SCHEDULER * sched, int cliente);




int main(){

    SCHEDULER sched;

    sched.listaPCB = NULL;
    pthread_mutex_init(&(sched.listaPCBMutex), NULL);
    sched.listaPCB_END = NULL;
    pthread_mutex_init(&(sched.listaPCB_ENDMutex), NULL);
    sched.tipo = RR;
    sched.quantum = 3;
    sched.enabled = 1;
    sched.PIDS = 0;

    pthread_t menuThread;
    pthread_create( &menuThread, NULL, menu, (void *) (&sched));
    pthread_t cpuSchedThread;
    pthread_create( &cpuSchedThread, NULL, CPUScheduler, (void *) (&sched));


    server(&sched);

    pthread_join( cpuSchedThread, NULL);
    pthread_join( menuThread, NULL);


    borrarLista(sched.listaPCB_END);


    //SCHEDULER sched;
    //server(&sched);

    return 0;
}


void server(SCHEDULER *sched){
    int socketServidor;
    int socketCliente[MAX_CLIENTS];
    int numeroClientes = 0;
    fd_set descriptoresLectura;
    int maximo;
    char servbuffer[BUFFERSIZE];
    int i;

    //Open server
    socketServidor = socket(AF_INET, SOCK_STREAM, 0);
    if (socketServidor == -1)
    {
        perror ("Error al abrir servidor");
        exit (EXIT_FAILURE);
    }
    //address host
    struct sockaddr_in server_add;
    server_add.sin_family = AF_INET;
    server_add.sin_port = htons(9002);
    server_add.sin_addr.s_addr = INADDR_ANY;

    //bind: hostea
    bind(socketServidor, (struct sockaddr*) &server_add, sizeof(server_add));
    //listen for conns
    listen(socketServidor, 10);

    printf("Servidor abierto: esperando clientes\n");

    while(active){
        compactKeys (socketCliente, &numeroClientes);
        FD_ZERO (&descriptoresLectura);
        //Set select()
        FD_SET (socketServidor, &descriptoresLectura);
        for (i=0; i<numeroClientes; i++)
            FD_SET (socketCliente[i], &descriptoresLectura);

        maximo = getMAx (socketCliente, numeroClientes);
        if (maximo < socketServidor)
            maximo = socketServidor;
        /* Espera indefinida hasta que alguno de los descriptores tenga algo
		 * que decir: un nuevo cliente o un cliente ya conectado que envía un
		 * mensaje */
        select (maximo + 1, &descriptoresLectura, NULL, NULL, NULL);

        /* Se comprueba si algún cliente ya conectado ha enviado algo */
        for (i=0; i<numeroClientes; i++)
        {
            if (FD_ISSET (socketCliente[i], &descriptoresLectura))
            {
                /* Se lee lo enviado por el cliente y se escribe en pantalla */
                if ( (recv(socketCliente[i], &servbuffer, BUFFERSIZE, 0) > 0)){

                    //printf ("Cliente %d envía proceso: %s\n", i+1, servbuffer);
                    nuevoProceso(servbuffer, sched, socketCliente[i]);
                }
                else
                {
                    /* Se indica que el cliente ha cerrado la conexión y se
                     * marca con -1 el descriptor para que compactKeys() lo
                     * elimine */

                    //printf ("*** SERVIDOR *** Cliente %d ha cerrado la conexión\n", i+1);
                    socketCliente[i] = -1;
                }
            }
        }

        /* Se comprueba si algún cliente nuevo desea conectarse y se le
         * admite */
        if (FD_ISSET (socketServidor, &descriptoresLectura))
            nuevoCliente (socketServidor, socketCliente, &numeroClientes);

    }

}

void nuevoCliente (int servidor, int *clientes, int *nClientes)
{
    /* Acepta la conexión con el cliente, guardándola en el array */
    clientes[*nClientes] = accept(servidor, NULL, NULL);
    (*nClientes)++;

    /* Si se ha superado el maximo de clientes, se cierra la conexión,
     * se deja to-do como estaba y se vuelve. */
    if ((*nClientes) >= MAX_CLIENTS)
    {
        close (clientes[(*nClientes) -1]);
        (*nClientes)--;
        return;
    }


    /* Escribe en pantalla que ha aceptado al cliente y vuelve */
    printf ("*** SERVIDOR *** Aceptado cliente %d\n", *nClientes);
    return;
}

void nuevoProceso(char * line, SCHEDULER * sched, int cliente){
    char * token;
    token = strtok(line, " ");
    int burst;
    burst = (int)strtol(token,(char **)NULL,10);

    token = strtok(NULL, " ");
    int pri;
    pri = (int)strtol(token,(char **)NULL,10);

    PCB * pcb = malloc(sizeof(PCB));
    pcb->burst = burst;
    pcb->burstActual = burst;
    pcb->endTime = NULL;
    pcb->pid = 0;
    pcb->priority = pri;
    pcb->quantum = 0;
    pcb->startTime = NULL;


    agregarPCB(sched, pcb);

    long id = pcb->pid;


    char server_message[BUFFERSIZE] = "Aceptado proceso (Burst: ";
    char nums[5];
    sprintf(nums,"%d",burst);
    strcat(server_message, nums);
    strcat(server_message, " - Prioridad: ");
    sprintf(nums,"%d",pri);
    strcat(server_message, nums);
    strcat(server_message, ") ---> PID asignado: ");
    sprintf(nums,"%ld",id);
    strcat(server_message, nums);

    //printf("%s\n", server_message);

    send(cliente, server_message, BUFFERSIZE, 0);


}

/* ----------------------
 * Management
 * ----------------------
 */


void changeActive(int act){
    active = act;
}

void compactKeys (int *tabla, int *n)
{
    int i,j;

    if ((tabla == NULL) || ((*n) == 0))
        return;

    j=0;
    for (i=0; i<(*n); i++)
    {
        if (tabla[i] != -1)
        {
            tabla[j] = tabla[i];
            j++;
        }
    }

    *n = j;
}

int getMAx (int *tabla, int n)
{
    int i;
    int max;

    if ((tabla == NULL) || (n<1))
        return 0;

    max = tabla[0];
    for (i=0; i<n; i++)
        if (tabla[i] > max)
            max = tabla[i];

    return max;
}





