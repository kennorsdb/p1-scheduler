//
// Created by alejo on 4/8/18.
//

#ifndef APPROACH_RESOURCES_H
#define APPROACH_RESOURCES_H

#include <pthread.h>
typedef struct pnode{
    pthread_t thread;
    struct pnode * next;
}PNODE;

typedef struct plist{
    int size;
    PNODE *head;
    PNODE *last;
} PLIST;

void newThread(PLIST * list, PNODE * node);

#endif //APPROACH_RESOURCES_H
