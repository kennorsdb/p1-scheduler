//
// Created by alejo on 4/2/18.
//

#include "cliente.h"
#include "resources.h"

#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define BUFFERSIZE 512

Process * lineParser(char *line);
FILE * readFile(int *head);
void manualInit();
void * autoInit(void * arg);
void * clientConnect(void * arg);
void launchThread(Process *process);
void manualProcess(FILE * fp, int skip);
void * autoProcess(void *arg);

PLIST activeThreads;
int aliveThreads = 0;

int autoActive = 1;

// /home/alejo/lines.txt


int main(int argc, char *argv[]) {
    printf("\033[H\033[J-------------------------------------------------------------------------------\n");
    printf("--   _____ _____ _____                                                       --\n");
    printf("--  |_   _|  ___/  __ \\   I N S T I T U T O   T E C N O L Ó G I C O          --\n");
    printf("--    | | | |__ | /  \\/   -------------------------------------------        --\n");
    printf("--    | | |  __|| |       ESCUELA DE COMPUTACIÓN                             --\n");
    printf("--    | | | |___| \\__/\\                                                      --\n");
    printf("--    \\_/ \\____/ \\____/                                                      --\n");
    printf("--                                                                           --\n");
    printf("-------------------------------------------------------------------------------\n");


    init();
    return 0;
}


void init(){
    srand ( time(NULL) );
    activeThreads.size = 0;

    int opcion;
    printf("Seleccione el tipo de CLIENTE:\n"
           "1. Manual\n2. Automático\n");
    printf(">>");
    scanf("%d",&opcion);
    pthread_t menuauto;
    switch(opcion){
        case 1:
            manualInit();
            break;
        case 2:
            autoInit(NULL);
            break;
        default:
            printf("Opción incorrecta!\n");
            return init();

    }
}

void manualInit(){

    char op='N';
    int flag = 1;
    FILE * fp;
    int skip;
    while(flag) {
        fp = readFile(&skip);
        if (fp == NULL) {
            printf("Fichero no encontrado\n");
            flag=0;
        } else {
            break;
        }
    }
    if(flag){
        manualProcess(fp, skip);
    }

}

void * autoInit(void * arg){

    int bMin;
    int BMax;
    int sMin;
    int sMax;
    printf("Valor minimo burst (>0):\n>>");
    scanf("%d", &bMin);
    printf("Valor maximo burst:\n>>");
    scanf("%d", &BMax);
    printf("Tasa de creación mínima (>0):\n>>");
    scanf("%d", &sMin);
    printf("Tasa de creación maxima:\n>>");
    scanf("%d", &sMax);

    Autoparam *args = malloc(sizeof(Autoparam));
    args->bMin = bMin;
    args->BMax = BMax;
    args->sMin = sMin;
    args->sMax = sMax;

    pthread_t autoLoop;
    printf("------------------------\n");
    printf("Ingrese 'e' para detener\n");
    printf("------------------------\n");
    pthread_create(&autoLoop,NULL, autoProcess,args);
    char option = '\0';
    while( option != 'e') {
        option = '\0';
        scanf("%1c", &option);
        switch (option) {
            case 'e':
                printf("Cliente detenido\n");
                autoActive = 0;
                break;
            default:
                break;
        }
    }

    pthread_join(autoLoop,NULL);
    free(args);

}

/******************************
 * Funciones para cliente manual
 ******************************/

FILE * readFile(int *head){
    char path[500];
    printf("Ruta del fichero:\n>>");
    scanf("%s", path);
    printf("Header lines:\n>>");
    scanf("%d", head);

    FILE * fp;
    fp = fopen(path, "r");
    return fp;

}

void manualProcess(FILE * fp, int skip){
    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    int lineNumber = 0;
    //Skip header
    for(int i=0; i<skip; i++){
        read = getline(&line, &len, fp);
        if(read==-1){
            printf("Header excede tamaño de archivo\n");
            exit(EXIT_FAILURE);
        }
        lineNumber++;
    }

    Process * process;
    while ((read = getline(&line, &len, fp)) != -1) {
        // LOGICA DEL LOOP DE PROCESO
        lineNumber++;
        Process *tmp = lineParser(line);
        if(tmp!=NULL){
            process = malloc(sizeof(Process));
            process->burst = tmp->burst;
            process->prioridad = tmp->prioridad;
            printf("Enviando proceso. Burst: %d - Prioridad: %d\n",process->burst,process->prioridad);

            launchThread(process);
            int sleept = rand() % (8 + 1 - 3) + 3;
            sleep(sleept);

        }
        else{
            printf("Error en la linea: %d. No se puede crear proceso\n",lineNumber);
        }
    }

    fclose(fp);
    if (line)
        free(line);




    printf("Todos los procesos fueron enviados\n");
    printf("Total: %d\n",activeThreads.size);

    printf("Esperando que los demás threads terminen...\n");

    pthread_join((activeThreads.last->thread), NULL);

    free(process);

}

Process * lineParser(char *line){
    char * token;
    int matches = 0;
    static Process proc;
    token = strtok(line, " ");
    if(token!=NULL){
        int value;
        value = (int)strtol(token,(char **)NULL,10);
        if(value){
            matches++;
            proc.burst = value;
        }
        else
            return NULL;
    }
    else
        return NULL;
    token = strtok(NULL, " ");
    if(token!=NULL){
        int pro;
        pro = (int)strtol(token,(char **)NULL,10);
        if(pro){
            matches++;
            proc.prioridad = pro;
        }
        else
            return NULL;
    }
    else
        return NULL;
    while( (strtok(NULL, " "))!=NULL) {
        matches++;
    }
    if(matches>2){
        return NULL;
    }
    return &proc;
}


/****************************
* Funciones para cliente auto
****************************/

void * autoProcess(void *arg){

    Autoparam *datos;
    datos = (Autoparam *) arg;

    int bMin = datos->bMin;
    int BMax = datos->BMax;
    int sMin = datos->sMin;
    int sMax = datos->sMax;


    int procNumber = 0;

    Process *process;
    while (autoActive) {
        // LOGICA DEL LOOP DE PROCESO
        int burst = rand() % (BMax + 1 - bMin) + bMin;
        int priori = rand() % (5 + 1 - 1) + 1;
        process = malloc(sizeof(Process));
        process->burst = burst;
        process->prioridad = priori;

        if(&process!=NULL){
            printf("Enviando proceso. Burst: %d - Prioridad: %d\n",process->burst,process->prioridad);

            launchThread(process);
            int s = rand() % (sMax + 1 - sMin) + sMin;
            sleep(s);

        }
        else{
            printf("Error al crear proceso automático\n");
        }
    }

    printf("Esperando que los demás threads finalicen...\n");


    pthread_join((activeThreads.last->thread), NULL);


    free(process);
}


/****************************
* Funciones threads y sockets
****************************/

void launchThread(Process  *process){
    PNODE * pnode = malloc(sizeof(PNODE));
    newThread(&activeThreads, pnode);
    int rc = pthread_create(&(activeThreads.last->thread), NULL, clientConnect, process);
    if (rc){
        printf("ERROR; return code from pthread_create() is %d\n", rc);
    }

}

void * clientConnect(void * arg) {

    int socketest;
    socketest =  socket(AF_INET, SOCK_STREAM, 0);

    //address
    struct sockaddr_in ser_add;
    ser_add.sin_family = AF_INET;
    ser_add.sin_port = htons(9002);
    ser_add.sin_addr.s_addr = INADDR_ANY;

    //connection
    int connection = connect(socketest, (struct sockaddr *) &ser_add, sizeof(ser_add));

    if(connection==-1){
        printf("Error al hacer la conexión\n");
        return NULL;                                //Buscar como matar el main thread si ningun proceso se puede conectar
    }

    else{
        //Parse argument and create buffer
        Process * datos;
        datos = (Process *)arg;


        int burst = datos->burst;
        int prio = datos->prioridad;
        char ser_resp[BUFFERSIZE];


        char nums[5];
        sprintf(nums,"%d",burst);
        strcpy(ser_resp, nums);
        strcat(ser_resp, " ");
        sprintf(nums,"%d",prio);
        strcat(ser_resp, nums);

        // Arma string "burst priori"

        //printf("%s\n",ser_resp);

        send(socketest, ser_resp, BUFFERSIZE, 0);


        //printf("Esperando resp\n");
        char ser_ans[BUFFERSIZE];

        recv(socketest, &ser_ans, BUFFERSIZE, 0);

        //read(socketest, &ser_resp, BUFFERSIZE);

        printf("R: %s\n", ser_ans);


    }
    //close
    close(socketest);
}






