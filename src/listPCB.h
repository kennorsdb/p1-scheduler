#ifndef nodoPCB_FILE
#define nodoPCB_FILE

#include <pthread.h>
#include <time.h>

typedef struct
{
    long pid;
    long burst;
    long burstActual;
    long priority;
    long quantum;
    time_t startTime;
    time_t endTime;
} PCB;

typedef struct NODO_PCB NODO_PCB;
struct NODO_PCB
{
    PCB *pcb;
    NODO_PCB *sig;
    NODO_PCB *ant;
};

enum SCHEDULER_TYPE
{
    FIFO,
    SJF,
    HPF,
    RR
};

typedef struct
{
    NODO_PCB *listaPCB;
    pthread_mutex_t listaPCBMutex;
    NODO_PCB *listaPCB_END;
    pthread_mutex_t listaPCB_ENDMutex;
    enum SCHEDULER_TYPE tipo;
    long quantum;
    int enabled;
    long procesosEjecutados;
    time_t timeStart;
    time_t timeStop;
    int PIDS;
} SCHEDULER;
void agregarPCB(SCHEDULER * sched, PCB *pcb);
void printnodoPCB(NODO_PCB *nodoPCB);
void borrarLista(NODO_PCB *nodoPCB);
void * CPUScheduler(void *scheduler);
#endif